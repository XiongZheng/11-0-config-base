package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
class PropertyTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_config() throws Exception {
        mockMvc.perform(get("/name"))
                .andExpect(content().string("This is really config"));
    }

    @Test
    void should_return_root() throws Exception {
        mockMvc.perform(get("/password"))
                .andExpect(content().string("root"));
    }

    @Test
    void should_return_helloWorld() throws Exception {
        mockMvc.perform(get("/start"))
                .andExpect(content().string("HelloWorld"));
    }
}

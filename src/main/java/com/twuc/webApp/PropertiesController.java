package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertiesController {

    @Value("${twuc.webapp.name}")
    private String name;

    @Value("${twuc.webapp.password}")
    private String password;

    @Value("${twuc.webapp.start}")
    private String start;

    @Autowired
    private Configuration configuration;


    @GetMapping("/start")
    public String getHello(){
        return configuration.getStart();
    }

    @GetMapping("/name")
    public String getName(){
        return configuration.getName();
    }

    @GetMapping("/password")
    public String getRoot(){
        return configuration.getPassword();
    }
}
